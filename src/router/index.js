import Vue from 'vue'
import VueRouter from 'vue-router'
import {getToken} from '../utils/auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Index',
    component: () => import(/* webpackChunkName: "about" */ '../views/Index.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "about" */ '../views/login.vue')
  },
  {
    path: '/disk/:file_path',
    name: 'self_page',
    component: () => import( /* webpackChunkName: 'self_page' */ '../views/_self.vue')
  },
  {
    path: '*', component: () => import('../views/error.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach((to, from, next) => {
  let whiteUrl = ['/', '/login']
  if (!whiteUrl.includes(to.path)) {
    if (!getToken()) {
      let redUrl = '/login'
      // if (to.fullPath !== '/disk/1213') {
      //  let redUrl = '/login?redirect=' + to.fullPath
      // }
      next(redUrl)
    } else {
      next()
    }
  }
  next()
})

export default router
