import axios from "./index";

export function upload(data){
    return axios({
        url: '/api/work_main/sliceUpload',
        method: 'post',
        data: data,
        headers:{
            'Content-Type': 'multipart/form-data'
        }
    })
}


export function merge(fileMD5, name, key){
    let data = new FormData()
    data.append('key', key)
    data.append('filename', name)
    return axios({
        url: `/api/work_main/mergeUpload`,
        method: 'post',
        data: data
        // headers: {
        //     'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        // }   
    })
}


export function getFileList(limit, offset){
    return axios({
        url: `/api/work_main/fileAction?limit=${limit}&offset=${offset}`,
        method: 'get'
    })
}



export function ver(data){
    return axios({
        url: `/api/work_main/verificationCapacity`,
        method: 'post',
        data: data
    })
}


export function getFile(md5){
    let formData = new FormData();
    formData.append('file_md5', md5)
    return axios({
        url: `/api/work_main/getFile`,
        // url: `/work_main/download?file_md5=${md5}`,
        method: 'post',
        data: formData
    })
}


export function changeFile(md5, file_name){
    let formData = new FormData();
    formData.append('file_md5', md5)
    formData.append('file_name', file_name)
    return axios({
        url: `/api/work_main/fileAction`,
        method: 'post',
        data: formData
    })
}


export function removeFile(md5) {
    return axios({
        url: `/api/work_main/fileAction?file_md5=${md5}`,
        method: 'delete'
    })
}


export function changeFileStatus(md5){
    let formData = new FormData();
    formData.append('md5', md5)
    return axios({
        url: `/api/work_main/change_status`,
        method: 'post',
        data: formData
    })
}