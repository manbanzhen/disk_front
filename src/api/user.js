import axios from './index'

export function login(data) {
    return axios({
        url: '/api/user/login',
        method: 'post',
        data: data
    })
}


export function getInfo(){
    return axios({
        url: `/api/user/profile`,
        method: 'get'
    })
}