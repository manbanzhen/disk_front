import Axios from 'axios'
import {getToken} from "../utils/auth"

// Axios.defaults.baseURL = 'http://127.0.0.1:8000'
Axios.defaults.baseURL = 'http://home.zhanghaoran.ren:9000'
// CORS_ALLOW_HEADERS

Axios.interceptors.request.use(
    config => {
            config.headers['AUTHORIZATION'] = getToken()
        return config
    },error => {
        return Promise.reject(error)
    }
)


Axios.interceptors.response.use(
    response => {
        if (response.data.code === 0) {
            return response.data.data
        } else if (response.data.code === 300){
            alert(response.data.data[0].file_name + '已存在')
            return Promise.reject(response.data)
        }else {
            console.log(response.data.error)
            return Promise.reject(response.data.error)
        }
    },
    error => {
        return Promise.reject(error.response)
    }
)
export default Axios