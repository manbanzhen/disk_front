import cookie from 'js-cookie'


export function getToken (){
    return cookie.get('token')
}


export function setUserInfo (user) {
    cookie.set('token', user.token)
    cookie.set('file_path', user.file_path)
}


export function logout () {
    cookie.remove('token')
    cookie.remove('file_path')
}