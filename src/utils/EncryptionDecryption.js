import Vue from 'vue'
import CryptoJS from 'crypto-js'

export default {
    encrypt (word, keyStr) {
        keyStr = keyStr ? keyStr : 'zhanghaoranniubi666'
        let key = CryptoJS.enc.Utf8.parse(keyStr)
        let srcs = CryptoJS.enc.Utf8.parse(word)
        let encrypted = CryptoJS.AES.encrypt(srcs, key, {mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7})
        // let encJson = encrypted.toString()
        // return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(encJson))
        return encrypted
    },

    decrypt (word, keyStr) {
        keyStr = keyStr ? keyStr : 'zhanghaoranniubi666'
        let key = CryptoJS.enc.Utf8.parse(keyStr)
        let srcs = CryptoJS.enc.Base64.parse(word).toString(CryptoJS.enc.Utf8)
        console.log(srcs)
        let decrypt = CryptoJS.AES.decrypt(srcs, key, {mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7})
        return CryptoJS.enc.Utf8.stringify(decrypt).toString()
    }
}