import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './assets/common.css'

Vue.config.productionTip = false

import {Button, Form, FormItem, Input, Progress, Dialog, Message, Pagination, MessageBox, Table, TableColumn, Dropdown, DropdownItem, DropdownMenu} from "element-ui";

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Progress)
Vue.use(Dialog)
Vue.use(Pagination)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Dropdown)
Vue.use(DropdownItem)
Vue.use(DropdownMenu)

const MsgBox = MessageBox;
Vue.prototype.$prompt = MsgBox.prompt;
Vue.prototype.$alert = MsgBox.alert;
Vue.prototype.$message = Message
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
