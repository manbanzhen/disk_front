module.exports = {
    // devServer: {
    //   proxy: {
    //       '/api': {
    //           // target: 'http://home.zhanghaoran.ren:9000',
    //           target: 'http://127.0.0.1:8000',
    //           changeOrigin: true
    //       }
    //   }
    // }
  configureWebpack: (config)=>{
    if(process.env.NODE_ENV === 'production'){
      config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
    }
  }
  }